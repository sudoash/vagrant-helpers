Vagrant provisioner
=========

This repository will install Ubuntu 14.04, with PHP 5.5, Apache a few basic PHP extensions.

Just run the command below to download the latest Vagrant file and provisioner.

```sh
curl -L -o 'install.sh' https://bitbucket.org/sudoash/vagrant-helpers/raw/master/install.sh && curl -L -o 'Vagrantfile' https://bitbucket.org/sudoash/vagrant-helpers/raw/master/Vagrantfile && vagrant up
```
    